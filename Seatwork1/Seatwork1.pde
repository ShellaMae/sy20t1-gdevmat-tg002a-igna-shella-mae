void setup()
{
  size(1080,720,P3D);
  camera(0,0,-(height/2)/tan(PI*30/180),
         0,0,0,
         0,-1,0);
}

Walker walker = new Walker();

void draw(){
  float randomValue=random(4);
  
  stroke(random(255), random(255), random(255), random(255));
  fill(random(255), random(255), random(255), random(255));
  println(randomValue);
  walker.render();
  walker.randomWalk();
}
