void setup(){
   size(1080,720,P3D);
   background(255);
   camera(0,0,-(height/2)/tan(PI*30/180),
         0,0,0,
         0,-1,0);
}
float y;
void draw(){
  
  float mean = 0;
  float stdDev = 100;
  float gauss = randomGaussian(); //normal distribution
  
  float x = stdDev * gauss + mean;
  y=int(random(height));
  
  noStroke();
  fill(random(255),random(255),random(255));
  circle(x,y,x);
  
}

/*
  gaussian - normal distribution
  distribution of values that cluster around an average
  
  two key components - mean & standard deviation
*/
