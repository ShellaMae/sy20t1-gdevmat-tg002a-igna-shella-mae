void setup()
{
  size(1920,1080,P3D);
  camera(0,0,-(height/2)/tan(PI*30/180),
         0,0,0,
         0,-1,0);
      
  w = width+16;
  dx = (TWO_PI / period) * xspacing;
  yvalues = new float[w/xspacing];
}

int y;

void draw()
{
  background(130);
  drawCartesianPlane();
  drawLinearFunction();
  drawQuadraticFunction();
  drawCircle();
  calcWave();
  renderWave();

}

void drawCartesianPlane()
{
  stroke(0,0,0);
  line(300,0,-300,0);
  line(0,300,0,-300);
  
  for(int i=-300;i<=300;i+=10)
  {
    line(i,-5,i,5);
    line(-5,i,5,i);
  }
}

void drawLinearFunction()
{
  stroke(250,0,0);
  /*
  f(x)=x+2;
  Let x be 4, then y=6(4,6)
  Let x be -5,then y =-3(-5,-3)
  */
  
  for(int x=-200;x<=200;x++)
  {
    circle(x,x+2,1);
  }
}

void drawQuadraticFunction()
{
  stroke(0,0,255);
  /*
  f)x)=x^2+2x-5
  */
  
  for(float x=-300;x<=300;x+=0.1)
  {
    circle(x*10,(float)Math.pow(x,2)+(2*x)-5,1);
  }
}

 float radius=50;
void drawCircle()
{
  stroke(138,43,226);
  for(int x=0;x<=360;x++)
  {
   circle((float)Math.cos(x)*radius,(float)Math.sin(x)*radius,1);
  }
}

int xspacing = 16;   // How far apart should each horizontal location be spaced
int w;              // Width of entire wave

float theta = 0.0;  // Start angle at 0
float amplitude = 75.0;  // Height of wave
float period = 300.0;  // How many pixels before the wave repeats
float dx;  // Value for incrementing X, a function of period and xspacing
float[] yvalues;  // Using an array to store height values for the wave

void calcWave() {
  // Increment theta (try different values for 'angular velocity' here
  theta += 0.02;

  // For every x value, calculate a y value with sine function
  float x = theta;
  for (int i = 0; i < yvalues.length; i++) {
    yvalues[i] = sin(x)*amplitude;
    x+=dx;
  }
}

void renderWave() {
  noStroke();
  fill(255);
  ellipseMode(CENTER);
  // A simple way to draw the wave with an ellipse at each location
  for (int x = 0; x < yvalues.length; x++) {
    ellipse(x*xspacing, height/200+yvalues[x], 16, 16);
  }
}
//code from:https://processing.org/examples/additivewave.html
